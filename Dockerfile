FROM python:3-slim

# ENV \
#     # Unset ASPNETCORE_URLS from aspnet base image
#     ASPNETCORE_URLS= \
#     # Do not generate certificate
#     DOTNET_GENERATE_ASPNET_CERTIFICATE=false \
#     # SDK version
#     DOTNET_SDK_VERSION=5.0.405 \
#     # Enable correct mode for dotnet watch (only mode supported in a container)
#     DOTNET_USE_POLLING_FILE_WATCHER=true \
#     # Skip extraction of XML docs - generally not useful within an image/container - helps performance
#     NUGET_XMLDOC_MODE=skip \
#     # PowerShell telemetry for docker image usage
#     POWERSHELL_DISTRIBUTION_CHANNEL=PSDocker-DotnetSDK-Debian-11 \
#     PATH=$PATH:/usr/share/dotnet \
#     DOTNET_ROOT=/usr/share/dotnet

RUN apt-get update \
    && apt-get upgrade -y --no-install-recommends \
    && apt-get install -y --no-install-recommends \
        openssh-client \
        curl \
        git \
        procps \
        wget \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
# # Install .NET SDK
#     && curl -fSL --output dotnet.tar.gz https://dotnetcli.azureedge.net/dotnet/Sdk/$DOTNET_SDK_VERSION/dotnet-sdk-$DOTNET_SDK_VERSION-linux-x64.tar.gz \
#     && dotnet_sha512='be1b3b2c213937d5d17ed18c6bd3f8fab2d66593642caf14229d12f68ddfa304edb4d88ce735ee0347969dc79a9e3d7d8cddfb5ff2044177cda0f2072ed8bd47' \
#     && echo "$dotnet_sha512  dotnet.tar.gz" | sha512sum -c - \
#     && mkdir -p /usr/share/dotnet \
#     && tar -oxzf dotnet.tar.gz -C /usr/share/dotnet \
#     && rm dotnet.tar.gz \
#     && dotnet --info \
# Install Ansible
    && pip install --upgrade --no-cache-dir \
        pip \
        ansible \
        kubernetes \
    && ansible --version

WORKDIR /home

CMD ["python3"]
