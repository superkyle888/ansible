# ansible

This is made for Bitbucket pipeline speed up, where installing ansible every time takes time.

## Image Content

- Built from the official [python:3-slim](https://hub.docker.com/_/python).
- Includes the latest Ansible from pip install
- Includes related dependencies
