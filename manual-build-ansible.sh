#!/bin/bash
# set -o errexit

REPO=superkyle888/ansible
TAG=latest

docker build . -t ${REPO}:${TAG} \
               --build-arg http_proxy=http://192.168.100.102:10809 \
               --build-arg https_proxy=http://192.168.100.102:10809

docker push ${REPO}:${TAG}

VER=$(docker run --rm ${REPO}:${TAG} pip show ansible | grep Version: | awk '{print $2}')
docker tag ${REPO}:${TAG} ${REPO}:${VER}
docker push ${REPO}:${VER}

docker image ls --digests

# Remove the <none> tag image
docker stop $(docker ps | grep "${REPO}" | awk '{print $1}')
docker image rm $(docker images | grep "none" | awk '{print $3}')

docker image ls --digests